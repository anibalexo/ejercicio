
# Ejercicio Filtro de Alfabeto

# Dada una cadena formada sólo por caracteres en minúscula, cree dos métodos que
# filtren todas las consonantes y vocales de la palabra dada. Deben conservar el orden
# original de los caracteres en el resultado.

# Ejemplo:
# S= ’funeraria jaramillo’
# Retorno método filtro de vocales= ‘ueaiaaaio’
# Retorno método filtro de consonantes= ‘fnrrjrmll’

# Lenguajes de programación: Python 3 o JavaScript

# Enviar el link de cualquier repositorio GIT del ejercicio al siguiente correo: luis.mena@fuja.com.ec

var_ejem = 'funeraria jaramillo'

def filtro_vocales(parametro):
	resultado = ''	
	vocales = ('a','e','i','o','u')
	for crt in parametro:
		if crt in vocales:
			resultado += crt
	return resultado


def filtro_consonantes(parametro):
	resultado = ''
	vocales = ('a','e','i','o','u')
	for crt in parametro:
		if crt not in vocales:
			resultado += crt
	return resultado.replace(' ','')

def filtro(parametro):
	"metodo con las dos funcionalidades"
	resultado_voc = ''	
	resultado_con = ''	
	vocales = ('a','e','i','o','u')
	for crt in parametro:
		if crt in vocales:
			resultado_voc += crt
		else:
			resultado_con += crt
	return resultado_voc, resultado_con.replace(' ','')

print(filtro_vocales(var_ejem))
print(filtro_consonantes(var_ejem))
print(filtro(var_ejem))
